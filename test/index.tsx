import createStore, { IPreact, Connect } from '../src/ipreact'
import * as React from 'react'
import { createRoot } from 'react-dom/client'
const h = React.createElement

interface IStoreState {
    name?: string;
}
const { connect, dispatch, getState } = createStore<IStoreState>()({
    name: 'world'
})

interface AppProps {
    words?: string;
    prefix?: string;
}
const AppComponent = ({ words, prefix }: AppProps) => <h2>{prefix} {words}</h2>

const App = connect(() => {
    return {
        words: getState().name
    }
})(AppComponent)

let i = 0
setInterval(function () {
    const list = ['preact', 'immutable', 'world', 'ipreact']
     i = (i + 1) % list.length
    dispatch(state => ({...state, name: list[i]}))
}, 1000)


const root = createRoot(document.querySelector('#app'))
root.render(<App prefix="hello"/>)
